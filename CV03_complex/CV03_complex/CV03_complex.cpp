﻿// CV03_complex.cpp : Defines the entry point for the application.
//

#include "CV03_complex.h"

using namespace std;


double secti(double cislo1, double cislo2)
{
    return (cislo1 + cislo2);
}

int main()
{
    /* Deklarace promennych */
    double real1, imag1;
    double real2, imag2;

    /* Vytiskni hlavicku */
    printf("   Program secte 2 zadane komplexni cisla\n");
    printf("----------------------------------------------\n\n");
    printf("Cisla zadavejte ve formatu: <REAL> <IMAG>\n");

    /* Nacti data od uzivatele */
    printf("Zadejte 1. komplexni cislo: ");
    scanf_s("%lf %lfi", &real1, &imag1);
    printf("Zadejte 2. komplexni cislo: ");
    scanf_s("%lf %lfi", &real2, &imag2);

    /* Vytiskni zadana data pro kontrolu */
    printf("\n");
    printf("1. zadane cislo: %.1lf+(%.1lf)i\n", real1, imag1);
    printf("2. zadane cislo: %.1lf+(%.1lf)i\n", real2, imag2);

    /* funkce secti se postara o soucet 2 cisel */
    double soucetReal = secti(real1, real2);
    double soucetImag = secti(imag1, imag2);

    /* Vytiskni vysledek */
    printf("\n------------------------------------------------------\n");
    if (soucetImag > 0) 
    {
        printf("Vysledek souctu 2 komplexnich cisel: %.2lf+%.2lfi", soucetReal, soucetImag);
    }
    else 
    {
        printf("Vysledek souctu 2 komplexnich cisel: %.2lf%.2lfi", soucetReal, soucetImag);
    }

    /* Konec programu */
    printf("\n\n");
    system("pause");
    return 0;
}

