﻿// CV04_prestupny.cpp : Defines the entry point for the application.
//

#include "CV04_prestupny.h"

using namespace std;


/* Pokud se samotna funkce nenachazi nad funkci main */
/* musi byt definovana alespon primitivne */
bool jePrestupny(int rok);
bool jeLichy(int rok);

int main()
{
    printf("Program pro vypocet prestupneho roku\n\n");

    /* Nekonecna smycka - Kod v zavorce se bude porad dokola opakovat */
    while (1)
    {
        int rok;

        printf("Zadejte rok: ");
        scanf_s("%d", &rok);

        bool prestupny = jePrestupny(rok);

        if (prestupny == true)
        {
            printf("\t - JE prestupny\n");
        }
        else
        {
            printf("\t - NENI prestupny\n");
        }

        bool lichy = jeLichy(rok);

        if (lichy == true) 
        {
            printf("\t - JE lichy\n");
        }
        else 
        {
            printf("\t - JE sudy\n");
        }
    }
}

bool jePrestupny(int rok)
{
    int modulo4 = rok % 4;
    int modulo100 = rok % 100;
    int modulo400 = rok % 400;

    if ((modulo400 == 0) || ((modulo4 == 0) && (modulo100 != 0)))
    {
        return true;
    }

    return false;
}

bool jeLichy(int rok)
{
    return (bool)(rok % 2);
}


