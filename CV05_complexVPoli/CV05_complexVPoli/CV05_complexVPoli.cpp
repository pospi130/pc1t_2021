﻿// CV05_complexVPoli.cpp : Defines the entry point for the application.
//

#define _USE_MATH_DEFINES // To enable the M_PI macro

#include "CV05_complexVPoli.h"
#include<stdio.h>
#include<math.h>

using namespace std;

void soucet(double a[2], double b[2], double c[2])
{
	c[0] = a[0] + b[0];
	c[1] = a[1] + b[1];
}

void rozdil(double a[2], double b[2], double c[2])
{
	c[0] = a[0] - b[0];
	c[1] = a[1] - b[1];
}

void complexToPolar(double complex[2], double polar[2])
{
	double a = complex[0];
	double b = complex[1];
	double a2 = pow(a, 2);
	double b2 = pow(b, 2);

	/* Vypocet MODULU */
	double soucet_mocnin = a2 + b2;
	double modul_vysledek = sqrt(soucet_mocnin);

	/* Vypocet ARGUMENTU */
	double argument = atan(b / a) * (180.0 / M_PI);

	/* Ulozit vysledek do pole */
	polar[0] = modul_vysledek;
	polar[1] = argument;

	int q = 0;
	if (complex[0] >= 0 && complex[1] >= 0)
	{
		q = 1;
	}
	else if (complex[0] >= 0 && complex[1] < 0)
	{
		q = 2;
	}
	else if (complex[0] < 0 && complex[1] < 0)
	{
		polar[1] = -(180 - polar[1]);
		q = 3;
	}
	else if (complex[0] < 0 && complex[1] >= 0)
	{
		polar[1] = polar[1] + 180;
		q = 4;
	}

	printf("Kvadrant: %d\n", q);

}

void tisk(double complex[])
{
	if (complex[1] >= 0)
	{
		printf("%.1lf+%.1lfi", complex[0], complex[1]);
	}
	else
	{
		printf("%.1lf%.1lfi", complex[0], complex[1]);
	}
}

bool vypocitej_priklad(double a[], double b[], double c[], char op) 
{
	if (op == '+')
	{
		/******************** Soucet ******************/
		printf("Zvolena operace soucet.\n");

		/* Vytiskni promenne */
		// Prvni radek
		tisk(a); printf("\n");
		// Druhy radek
		tisk(b); printf("\n");

		/* Fuknce zajisti c=a+b */
		soucet(a, b, c);
	}
	else if (op == '-')
	{
		/******************** Rozdil ******************/
		printf("Zvolena operace odecet.\n");

		/* Vytiskni promenne */
		// Prvni radek
		tisk(a); printf("\n");
		// Druhy radek
		printf("-("); tisk(b); printf(")\n");

		/* Fuknce zajisti c=a-b */
		rozdil(a, b, c);
	}
	else
	{
		printf(" Spatny operator\n");
		return false;
	}

	printf("-----------\n");

	/* Vytiskni c (vysledek) */
	tisk(c);

	printf("\n\nVysledek je tedy: ");
	tisk(c);
	printf("\n");
	
	return true;
}

void doplnujiciUkol(double c[], double polar[])
{
	/* Doplnujici ukol */
	printf("\n\nDoplnujici UKOL:\nVysledek z KOMPLEX do POLAR formatu\n");
	complexToPolar(c, polar);

	if(polar[1] >= 0)
		printf("POLAR format: |%lf|+%.1lf stupnu\n\n", polar[0], polar[1]);
	else 
		printf("POLAR format: |%lf|%.1lf stupnu\n\n", polar[0], polar[1]);
}	

void nactiDataZKonzole(double a[], double b[], char *op) 
{
	printf("Zadavejte ve formatu <<REAL IMAG>>\n");
	printf("Zadej 1. komplexni cislo    : ");
	scanf_s("%lf %lfi", &a[0], &a[1]);
	printf("Zadejte operator << + / - >>: ");
	scanf_s(" %c", op, 1);
	printf("Zadej 2. komplexni cislo    : ");
	scanf_s("%lf %lfi", &b[0], &b[1]);
	printf("\n");
}

int main()
{
	char op;
	double a[2], b[2], c[2], polar[2];

	printf("Program pro soucet/rozdil komplexnich cisel\n\n");

	/* Uzivatel zada data*/
	nactiDataZKonzole(a, b, &op);

	if (!vypocitej_priklad(a, b, c, op)) 
	{
		printf("Vzniknula chyba programu...\n");
		return 1;
	}
	
	doplnujiciUkol(c, polar);

	system("pause");
	return 0;
}