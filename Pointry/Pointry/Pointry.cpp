﻿// Pointry.cpp : Defines the entry point for the application.
//

#include "Pointry.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

/* Vytvori se lokalni promenna "prom", 
   do ktere se naplni hodnota predana v parametru */
/* Tato lokalni promenna se po skonceni funkce odstrani
   Proto, ikdyz promennou inkrementuji o hodnotu 1, 
   ve funci main se to neprojevi 
*/
void pokus(int prom)
{
	printf("prom1: %d, addr: %p\n", prom, &prom);
	prom = prom + 1;
}

/* Vytvori se lokalni promenna (pointer, cesky ukazatel)
   a vlozi se do ni adresa predana pri volani funkce
   Na tuto adrefu pak funce zapisuje.
   Proto se inkrementace teto promenne projevi i ve funkci main
*/
void pokus2(int* prom)
{
	printf("prom2: %d, addr: %p\n", *prom, prom);
	/* Protoze pointer prom nenese hodnotu, ale adresu, 
	pro praci s hodnotami musim pridat hvezdicku */
	*prom = *prom + 1;
}

int main()
{
	int prom = 4;
	printf("prom: %d, addr: %p\n", prom, &prom);
	/* Predavani parametru hodnotou */
	pokus(prom);
	/* Funkce pokus hodnotu "prom" nezmenila */
	printf("prom: %d\n", prom);
	/* Predavani parametru odkazem (adresa, kde je hodnota ulzena) */
	/* Pokud funce pokus2 vyzaduje ukazatel, z promenne jej ziskam pomoci & */
	pokus2(&prom);
	printf("prom: %d\n", prom);


	return 0;
}