#include "modul.h"

int prunik(double start1, double stop1, double start2, double stop2, double* vysl_start, double* vysl_stop)
{
    int prunik_found = 0;

    if ((start1 <= stop1) && (start2 <= stop2) && (start1 < start2) && (stop1 >= start2))
    {
        prunik_found = 1;
        *vysl_start = start2;

        if (stop2 < stop1)
        {
            *vysl_stop = stop2;
            return prunik_found;
        }
        if (stop1 <= stop2)
        {
            *vysl_stop = stop1;
            return prunik_found;
        }
    }

    *vysl_start = 0;
    *vysl_stop = 0;

    return prunik_found;
}
