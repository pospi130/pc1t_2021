﻿// CV08_prunik.cpp : Defines the entry point for the application.
//

#include <stdio.h>
#include "modul.h"
#include "CV08_prunik.h"

using namespace std;

int main(int argc, const char* argv[])
{
    double vysl_start = 0, vysl_stop = 0;
    double zacatek1 = 0, zacatek2 = 0, konec1 = 0, konec2 = 0;

    while (1)
    {
        printf("Zadejte rozsahy (<cislo>,<cislo>) (<cislo>,<cislo>):\n");
        scanf_s("(%lf,%lf) (%lf,%lf)", &zacatek1, &konec1, &zacatek2, &konec2);

        printf("Zadano: [%.1lf, %.1lf] [%.1lf, %.1lf]\n", zacatek1, konec1, zacatek2, konec2);

        prunik(zacatek1, konec1, zacatek2, konec2, &vysl_start, &vysl_stop);

        printf("res: [%.1lf, %.1lf]\n\n", vysl_start, vysl_stop);

        while (getchar() != '\n');
    }
    return 0;
}
