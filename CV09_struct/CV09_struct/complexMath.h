#pragma once
#define _USE_MATH_DEFINES
#include<math.h>

typedef struct complex_t{
	double real;
	double imag;
}complex;

typedef struct complex_geom_t
{
	double modul;
	double argument;
}complex_geom;

typedef enum operace_e
{
	scitani = '+',
	odcitani = '-',
	nasobeni = '*',
}operace;

typedef enum format_e 
{
	geometricky,
	algebraicky
}format;



complex vypocet(complex *c1, complex *c2, operace *op);
void    tisk(complex* num, format form);
int     porovnej_cisla(complex* c1, complex* c2);