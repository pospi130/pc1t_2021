#include <stdio.h>
#include "complexMath.h"


static complex _scitani(complex* c1, complex* c2)
{
	complex result;
	result.real = c1->real + c2->real;
	result.imag = c1->imag + c2->imag;

	return result;
}

static complex _odcitani(complex* c1, complex* c2)
{
	complex result;
	result.real = c1->real - c2->real;
	result.imag = c1->imag - c2->imag;

	return result;
}

static complex_geom_t prevod_na_geometricky_tvar(complex* cislo)
{
	complex_geom result = { 0 };

	/* Vypocet MODULU */
	result.modul = sqrt(pow(cislo->real, 2) + pow(cislo->imag, 2));

	/* Vypocet ARGUMENTU */
	result.argument = atan(cislo->imag / cislo->real) * (180.0 / M_PI);

	return result;
}

complex vypocet(complex* c1, complex* c2, operace* op) 
{
	complex result = { 0 };

	switch (*op)
	{
	case scitani:
		printf("Operace scitani\n");
		return _scitani(c1, c2);
		break;
	case odcitani:
		printf("Operace odcitani\n");
		return _odcitani(c1, c2);
		break;
	default:
		printf("Zadany spatny operator\n");
	};

	return result;
}

int porovnej_cisla(complex* c1, complex* c2) 
{
	complex_geom_t c1_g = prevod_na_geometricky_tvar(c1);
	complex_geom_t c2_g = prevod_na_geometricky_tvar(c2);

	if (c1_g.modul > c2_g.modul) 
	{
		return 1;
	}
	else if (c1_g.modul == c2_g.modul) 
	{
		return 0;
	}
	
	return -1;
}

void tisk(complex* num, format form) 
{
	complex_geom geom = {0};

	switch (form)
	{
	case algebraicky:
		printf("Algebraicky vysledek:\n");
		printf("%.1lf%c%.1lfi\n", num->real, num->imag > 0 ? '+' : '\0', num->imag);
		break;
	case geometricky:
		printf("Geometricky vysledek:\n");
		geom = prevod_na_geometricky_tvar(num);
		printf("%.1lf*e^j%.1lfdeg\n", geom.modul, geom.argument);
		break;
	default:
		printf("Error\n");
		break;
	};


}