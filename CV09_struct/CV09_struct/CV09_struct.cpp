﻿// CV09_struct.cpp : Defines the entry point for the application.
//

#include <stdio.h>
#include "CV09_struct.h"
#include "complexMath.h"

using namespace std;

int main()
{
	operace op;
	char op_ch;
	complex c1       = { 0 };
	complex c2       = { 0 };
	complex vysledek = { 0 };

	printf("CV09_struct_enum project.\n");
	
	printf("Zadejte 1. komplexni cislo <<real imag>>: ");
	scanf_s("%lf %lf", &c1.real, &c1.imag);
	printf("Zadejte opreraci << + / - >>: ");
	scanf_s(" %c", &op_ch, 1);
	op = (operace)op_ch;
	printf("Zadejte 2. komplexni cislo <<real imag>>: ");
	scanf_s("%lf %lf", &c2.real, &c2.imag);
	
	printf("\n----------------------------------------------\n");
	printf("Zadano (c1): %.1lf%c%.1lfi\n", c1.real, c1.imag > 0 ? '+' : '\0', c1.imag);
	printf("Operace: %c\n", op);
	printf("Zadano (c2): %.1lf%c%.1lfi\n", c2.real, c2.imag > 0 ? '+' : '\0', c2.imag);

	printf("\n----------------------------------------------\n");
	printf("\nVypocet:\n");

	vysledek = vypocet(&c1, &c2, &op);

	tisk(&vysledek, algebraicky);
	tisk(&vysledek, geometricky);

	printf("Vysledek porovnani cisel: \n%d\n", porovnej_cisla(&c1, &c2));

	printf("\n\n");
	system("pause");

	return 0;
}

