﻿// CV03_cmake.cpp : Defines the entry point for the application.
//

#include "main.h"

using namespace std;

/* Funkce prebere parametr cas a navrati vypocitanou vzdalenost */
double spoctiVzdalenost(double cas)
{
    /* Definuji rychlost zvuku */
    double rychlost_zvuku = 330.0;

    /* Funkce vrati vysledek */
    return (cas * rychlost_zvuku);
}

int main()
{
    /* Deklarace promenne "cas" */
    double cas;

    /* Nacti data od uzivatele */
    printf("Zadejte cas v sekundach: ");
    scanf_s("%lf", &cas);

    /* Vysledek vystup funkce ulozen do promenne "vysledek" */
    double vysledek = spoctiVzdalenost(cas);

    /* Vypsat vysledek do konzole */
    printf("Blesk uhodil ve vzdalenosti %.2lf m.", vysledek);

    printf("\n\n");
    system("pause");
    return 0;
}
