#include "Body2d.h"
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

static int   findMaxLenOf(int velikost, int(*poleBodu)[2], int index);
static char* stringWithFixedWidth(int num, int num_of_digits);
static int   numOfPositions(int num);

int nearestIndex(int velikost, int(*poleBodu)[2], int referencniX, int referencniY)
{
	int    index = -1;
	double min_distance = INT_MAX;

	for (int i = 0; i < velikost; i++)
	{
		// Count the coordination difference
		int x_diff = referencniX - poleBodu[i][0];
		int y_diff = referencniY - poleBodu[i][1];

		// Count the distance
		double distance = sqrt(pow(x_diff, 2) + pow(y_diff, 2));

		//printf("%d. [%d,%d]\t vzdalenost = %.1lf\n", i, poleBodu[i][0], poleBodu[i][1], distance);

		// Find the minimal distance
		if (distance < min_distance)
		{
			//printf("Vzdalenost override: old = %.1lf, current: %.1lf", min_vzdalenost, vzdalenost);
			min_distance = distance;
			index = i;
		}

		//printf("\n\n");	
	}

	return index;
}

double averageDistance(int velikost, int(*poleBodu)[2], int referencniX, int referencniY)
{
	double sum = 0;

	// Count the distance for each coordination and make sum of all distances
	for (int i = 0; i < velikost; i++)
	{
		// Count the coordination difference
		int x_diff = referencniX - poleBodu[i][0];
		int y_diff = referencniY - poleBodu[i][1];

		// Count the distance
		double vzdalenost = sqrt(pow(x_diff, 2) + pow(y_diff, 2));

		sum += vzdalenost;

		//printf("%d. prum = %lf, appending vzdalenost = %.1lf\n", i, prum, vzdalenost);
	}

	// Divide the sum by the count to make the average value
	double average = sum / velikost;

	return average;
}

void printCoordinatesInRange(int velikost, int(*poleBodu)[2], int referencniX, int referencniY, int max_dist)
{
	int max_len_x = findMaxLenOf(velikost, poleBodu, 0);
	int max_len_y = findMaxLenOf(velikost, poleBodu, 1);

	// Count the distance for each coordination and make sum of all distances
	for (int i = 0; i < velikost; i++)
	{
		// Count the coordination difference
		int x_diff = referencniX - poleBodu[i][0];
		int y_diff = referencniY - poleBodu[i][1];

		// Count the distance
		double vzdalenost = sqrt(pow(x_diff, 2) + pow(y_diff, 2));

		if (vzdalenost < max_dist)
		{
			printf("%s. [", stringWithFixedWidth(i, numOfPositions(velikost)));
			printf("%s", stringWithFixedWidth(poleBodu[i][0], max_len_x));
			printf(",%s], dist: %.1lf\n", stringWithFixedWidth(poleBodu[i][1], max_len_y), vzdalenost);
		}
	}
}

// Static to make the function local only
static int numOfPositions(int num)
{
	// Allocate the space for number to be converted from int to string
	static char pole[10];
	// Convert the number from int to string
	sprintf(pole, "%d", num);
	// Return the length of the string
	return strlen(pole);
}

// Static to make the function local only
static char* stringWithFixedWidth(int num, int num_of_digits)
{
	// Count how many spaces is needed
	int diff_size = num_of_digits - numOfPositions(num);
	// Allocate the space for converting int num into string
	char num_char[10];
	// Create STATIC string - it won't be bestroyed when function ends
	static char text[10];

	// Empty the string
	memset(text, 0, sizeof(text));

	// Fill with needed spaces
	for (int i = 0; i < diff_size; i++)
	{
		strcat(text, " ");
	}

	// Convert int to string
	sprintf(num_char, "%d", num);
	// Append the number to the rest of the text (the spaces)
	strcat(text, num_char);

	return text;
}

static int findMaxLenOf(int velikost, int(*poleBodu)[2], int index)
{
	int max_len = 0;

	// Find the number wiht max number of digits in X and Y
	for (int i = 0; i < velikost; i++)
	{
		int current = poleBodu[i][index];
		int current_len = numOfPositions(current);

		// If current stored max num size is smaller than the new one, save it
		if (max_len < current_len)
		{
			max_len = current_len;
		}
	}

	return max_len;
}

void printCoordinates(int velikost, int(*poleBodu)[2])
{
	int max_len_x = findMaxLenOf(velikost, poleBodu, 0);
	int max_len_y = findMaxLenOf(velikost, poleBodu, 1);
	int num_pos_size = numOfPositions(velikost);

	// Print all the values with fixed positions - empty spaces
	for (int i = 0; i < velikost; i++)
	{
		printf("%s. [", stringWithFixedWidth(i, num_pos_size));
		printf("%s", stringWithFixedWidth(poleBodu[i][0], max_len_x));
		printf(",%s]\n", stringWithFixedWidth(poleBodu[i][1], max_len_y));
	}
	printf("\n");
}