﻿// CV07_body2d.cpp : Defines the entry point for the application.
//

#include "CV07_body2d.h"
#include "Body2d.h"
#include <stdio.h>

using namespace std;

#define COORDINATE_COUNT 10

int main()
{
	printf("  CV07 program example\n");
	printf("-----------------------\n\n");

	int coordinates[COORDINATE_COUNT][2] =
	{
		  5, 10,
		-14,  2,
		 45,  4,
		 -9,  8,
		 58, -3,
		 47,  5,
		-18,-86,
		 50,  7,
		-25, 51,
		 17, 98,
	};

	printCoordinates(COORDINATE_COUNT, coordinates);

	int referenceX;
	int referenceY;
	int max_dist;

	printf("Type reference point [x, y]: ");
	scanf_s("%d %d", &referenceX, &referenceY);
	printf("Type max range: ");
	scanf_s("%d", &max_dist);

	printf("referenceX,Y: [%d,%d], max_range: %d\n", referenceX, referenceY, max_dist);
	printf("---------------------------------------\n\n");

	int    ind_nearest = nearestIndex(COORDINATE_COUNT, coordinates, referenceX, referenceY);
	double average_dist = averageDistance(COORDINATE_COUNT, coordinates, referenceX, referenceY);


	printf("ind_nearest : %d -> [%d,%d]\n", ind_nearest, coordinates[ind_nearest][0], coordinates[ind_nearest][1]);
	printf("average_dist: %.2lf\n", average_dist);

	printf("\nAll coorfinates in distance of %d\n", max_dist);
	printCoordinatesInRange(COORDINATE_COUNT, coordinates, referenceX, referenceY, max_dist);

	system("pause");

	return 0;
}
