#pragma once

int    nearestIndex           (int velikost, int(*poleBodu)[2], int referencniX, int referencniY);
double averageDistance        (int velikost, int(*poleBodu)[2], int referencniX, int referencniY);
void   printCoordinates       (int velikost, int(*poleBodu)[2]);
void   printCoordinatesInRange(int velikost, int(*poleBodu)[2], int referencniX, int referencniY, int max_dist);