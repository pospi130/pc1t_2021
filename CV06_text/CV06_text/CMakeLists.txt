﻿# CMakeList.txt : CMake project for CV06_text, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

# Add source to this project's executable.
add_executable (CV06_text "CV06_text.cpp" "CV06_text.h" "statistika.h" "statistika.cpp")

# TODO: Add tests and install targets if needed.
