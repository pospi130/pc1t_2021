#pragma once
#include <ctype.h>
#define NULL	0

/* Funkce pocita pocet cislic v textu */
/* Pro nazornost dve moznosti zpusobu reseni */
int numOfDigits_pointer(char* text);
int numOfDigits_array(char pole[]);

/* Dunkce pocita pocet pismen v textu */
/* Pro nazornost dve moznosti zpusobu reseni */
int numberOfAlpha_pointer(char* text);
int numOfAlpha_array(char pole[]);

/* Funkce pocita pocet slov v zadanem textu */
int numOfWords_array(char pole[]);