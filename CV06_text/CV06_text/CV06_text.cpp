﻿// CV06_text.cpp : Defines the entry point for the application.
//

#include "CV06_text.h"
#include "statistika.h"

using namespace std;

int main()
{
	char text[] = "Toto jsou 2 vzorove vety, ktere budou zpracovavany \
Vasemi funkcemi. Auto Hyundai ix35 ma pres 120 konskych: sil.";

	int digitsCount = numOfDigits_pointer(text);
	int alphaCount = numberOfAlpha_pointer(text);
	int digitsCount2 = numOfDigits_array(text);
	int alphaCount2 = numOfAlpha_array(text);

	// Dplnujici ukol
	int wordCout = numOfWords_array(text);


	printf("Zadany text :\n%s\n\n", text);
	printf("Pocitani ruznymi metodami:\n");
	printf("---------------------------\n");
	printf("Pocet cisel :\t%d\n", digitsCount);
	printf("Pocet pismen:\t%d\n\n", alphaCount);
	printf("Pocet cisel :\t%d\n", digitsCount2);
	printf("Pocet pismen:\t%d\n\n", alphaCount2);
	printf("Pocet slov  :\t%d\n\n", wordCout);

	system("pause");
	return 0;
}
