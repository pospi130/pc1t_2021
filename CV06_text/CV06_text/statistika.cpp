#pragma once
#include "statistika.h"

int numOfDigits_pointer(char* text)
{
	int digitCount = 0;
	char valFromArray;

	while ((valFromArray = *text++) != NULL)
	{
		if (isdigit(valFromArray))
		{
			digitCount++;
		}
	}

	return digitCount;
}

int numOfAlpha_array(char pole[])
{
	int alphaCount = 0;

	for (int i = 0; pole[i] != NULL; i++)
	{
		if (isalpha(pole[i]))
		{
			alphaCount++;
		}
	}

	return alphaCount;
}

int numOfWords_array(char pole[])
{
	int  wordCount = 0;
	char currChar = 0;
	char prevChar = 0;

	for (int i = 0; (currChar = pole[i]) != NULL; i++)
	{
		if (isalnum(prevChar) && (ispunct(currChar) || isspace(currChar)))
		{
			wordCount++;
		}

		prevChar = pole[i];
	}

	return wordCount;
}

int numOfDigits_array(char pole[])
{
	int digitCount = 0;

	for (int i = 0; pole[i] != NULL; i++)
	{
		if (isdigit(pole[i]))
		{
			digitCount++;
		}
	}

	return digitCount;
}

int numberOfAlpha_pointer(char* text)
{
	int alphaCount = 0;
	char valFromArray;

	while ((valFromArray = *text++) != NULL)
	{
		if (isalpha(valFromArray))
		{
			alphaCount++;
		}
	}

	return alphaCount;
}