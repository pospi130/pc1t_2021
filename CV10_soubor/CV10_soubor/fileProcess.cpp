#include "fileProcess.h"

FILE* open_file(char* filepath, char* mode) 
{
    FILE* fptr;
    errno_t err;

    // use appropriate location if you are using MacOS or Linux
    err = fopen_s(&fptr, filepath, mode);

    if (fptr == NULL)
    {
        printf("Error opening the file!");
        system("pause");
        exit(1);
    }

   // printf("File successfully opened!\n");

    return fptr;
}

pocet alpha_num_count(FILE* fp) 
{
    int znak;
    int prev_znak = 0;
    pocet p = { 0 };

    while (1) 
    {
        znak = fgetc(fp);

        if (feof(fp))
        {
            break;
        }

        //printf("%c", (char)znak);

        if (isalpha((char)znak))
        {
            p.pismen++;
        }
        if (isdigit((char)znak))
        {
            p.cislic++;
        }

        if (isspace((char)znak)) 
        {
            if ((char)prev_znak != '.') 
            {
                p.slova++;
                //printf(" slovo (%d)", p.slova);
            }
        }

        if ((char)znak == '.')
        {
            p.slova++;
            p.vety++;
            //printf(" slovo (%d), veta", p.slova);
        }
        
        //printf("\n");

        prev_znak = znak;
    }

    return p;
}

void print_result(pocet * p) 
{  
    printf(VYSLEDEK_HEADER);
    printf(POCET_PISMEN, p->pismen);
    printf(POCET_CISLIC, p->cislic);
    printf(POCET_SLOV  , p->slova );
    printf(POCET_VET   , p->vety  );
}

int write_result_in_file(char * filename, pocet * p) 
{
    FILE * fp = open_file(filename, "w");

    fprintf(fp, VYSLEDEK_HEADER);
    fprintf(fp, POCET_PISMEN, p->pismen);
    fprintf(fp, POCET_CISLIC, p->cislic);
    fprintf(fp, POCET_SLOV  , p->slova );
    fprintf(fp, POCET_VET   , p->vety  );
        
   return close_file(fp);
}


int close_file(FILE* fp) 
{   
    //printf("\nClosing the file\n");
    return fclose(fp);
}