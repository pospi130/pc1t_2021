﻿# CMakeList.txt : CMake project for CV10_soubor, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

# Add source to this project's executable.
add_executable (CV10_soubor "CV10_soubor.cpp" "CV10_soubor.h" "fileProcess.cpp" "fileProcess.h")

# TODO: Add tests and install targets if needed.
