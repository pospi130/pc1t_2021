#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define VYSLEDEK_HEADER "Vysledek statistiky:\n"
#define POCET_PISMEN    "Pocet pismen: %d\n"
#define POCET_CISLIC    "Pocet cislic: %d\n"
#define POCET_SLOV      "Pocet slov  : %d\n"
#define POCET_VET       "Pocet vet   : %d\n"

typedef struct
{
    int pismen;
    int cislic;
    int slova;
    int vety;
}pocet;

FILE* open_file           (char* filepath, char* mode);
int   close_file          (FILE* fp);
pocet alpha_num_count     (FILE* fp);
void  print_result        (pocet* p);
int   write_result_in_file(char* filename, pocet* p);