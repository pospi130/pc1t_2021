﻿// CV10_soubor.cpp : Defines the entry point for the application.
//
#include <stdio.h>
#include "CV10_soubor.h"
#include "fileProcess.h"

using namespace std;

char* read_filepath         = "vstup.txt";
char* write_result_filepath = "vysledky.txt";

int main()
{
	printf("CV10 program\n");
	
	FILE* fptr = open_file(read_filepath, "r");
	pocet p    = alpha_num_count(fptr);

	printf("------------------------\n");
	print_result(&p);
	write_result_in_file(write_result_filepath, &p);
	printf("------------------------\n");

	close_file(fptr);
	
	system("pause");	
	return 0;
}
