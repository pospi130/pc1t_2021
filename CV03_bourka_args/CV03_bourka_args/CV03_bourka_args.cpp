﻿// CV03_bourka_args.cpp : Defines the entry point for the application.
//

#include "CV03_bourka_args.h"

using namespace std;

/* Funkce prebere parametr cas a navrati vypocitanou vzdalenost */
double spoctiVzdalenost(double cas)
{
    /* Definuji rychlost zvuku */
    double rychlost_zvuku = 330.0;

    /* Funkce vrati vysledek */
    return (cas * rychlost_zvuku);
}

int main(int argc, char **argv)
{
    /* Deklarace promenne "cas" */
    double cas;

    if (argc >= 2)
    {
        printf("Nacten argument s hodnotou: %s\n", argv[1]);
        // Argument mentioned
        cas = atof(argv[1]);
    }
    else 
    {
        printf("Argument pri startu nezadan\n\n");
        /* Nacti data od uzivatele */
        printf("Zadejte cas v sekundach: ");
        scanf_s("%lf", &cas);
    }
    
    /* Vysledek vystup funkce ulozen do promenne "vysledek" */
    double vysledek = spoctiVzdalenost(cas);

    /* Vypsat vysledek do konzole */
    printf("Blesk uhodil ve vzdalenosti %.2lf m.", vysledek);

    printf("\n\n");
    system("pause");
    return 0;
}
